const path = require('path');

module.exports = {
    outputDir: path.resolve(__dirname, 'html'),
    devServer: {
        proxy: {
            '/api': {
                target: 'https://api.generateur-qrcode.com',
                changeOrigin: true,
                pathRewrite: { '^/api': '' },
            },
        },
    },
};