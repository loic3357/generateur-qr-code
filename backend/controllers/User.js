const bcrypt = require('bcrypt');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const path = require('path');
const fs = require('fs');

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                password: hash,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                subscribe: false
            });
            user.save()
                .then(() => res.status(201).json({ message: 'Utilisateur créé !' }))
                .catch(error => {
                    if (error.code === 11000) {
                        // Error code 11000 is for duplicate key error
                        res.status(400).json({ error: 'Cet email est déjà utilisé.' });
                    } else if (error.errors && error.errors.email && error.errors.email.kind === 'unique') {
                        // Checking for unique validation error
                        res.status(400).json({ error: 'Cet email est déjà utilisé.' });
                    } else {
                        console.error('Error saving user:', error);
                        res.status(400).json({ error: 'Une erreur est survenue lors de l\'enregistrement.' });
                    }
                });
        })
        .catch(error => {
            console.error('Error hashing password:', error);
            res.status(500).json({ error: 'Une erreur est survenue lors du hachage du mot de passe.' });
        });
};

exports.login = (req, res, next) => {
    User.findOne({email: req.body.email})
        .then(user => {
            if (!user) {
                return res.status(401).json({error: 'Utilisateur non trouvé !'});
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({error: 'Mot de passe incorrect !'});
                    }
                    res.status(200).json({
                        user: user,
                        token: jwt.sign(
                            {userId: user._id},
                            'RANDOM_TOKEN_SECRET',
                            {expiresIn: '24h'}
                        )
                    });
                })
                .catch(error => res.status(500).json({error}));
        })
        .catch(error => res.status(500).json({error}));
};

exports.informations = (req, res, next) => {
    const userId = req.user._id; // Récupérez l'ID de l'utilisateur à partir de la session ou du JWT

    User.findById(userId)
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'Utilisateur non trouvé' });
            }
            res.status(200).json(user);
        })
        .catch(error => {
            res.status(500).json({ error: error.message });
        });
};

exports.putUser = async (req, res, next) => {
    try {
        console.log('Request body:', req.body); // Vérifiez les données reçues
        let user = await User.findById(req.body._id);

        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé' });
        }

        // Vérifiez si l'utilisateur a déjà une image
        if (user.imageURL) {
            const imagePath = path.join(__dirname, '../images', user.imageURL.split('/images/')[1]);

            // Vérifiez si le fichier existe sur le serveur
            if (fs.existsSync(imagePath)) {
                // Supprimer l'ancienne image si une nouvelle image est téléchargée
                if (req.file) {
                    fs.unlinkSync(imagePath); // Supprimer l'ancienne image du dossier "images"
                }
            } else {
                console.warn('Le fichier à remplacer n\'est déjà plus dans le serveur');
            }
        }

        // Mettre à jour l'URL de l'image si une nouvelle image est téléchargée
        if (req.file) {
            user.imageURL = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
        }

        // Mettre à jour d'autres champs
        user.email = req.body.email;
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.subscribe = req.body.subscribe;

        console.log('User before saving:', user); // Vérifiez les données avant d'enregistrer
        // Sauvegarder l'utilisateur mis à jour dans la base de données
        await user.save();

        res.status(200).json({ message: 'Utilisateur mis à jour avec succès', user });
    } catch (error) {
        console.error('Error updating user:', error);
        res.status(500).json({ error: error.message });
    }
};

exports.updateStripeCustomerId = async (req, res) => {
    const { userId, stripeCustomerId } = req.body;

    try {
        const updatedUser = await User.findByIdAndUpdate(userId, { stripeCustomerId }, { new: true });

        if (!updatedUser) {
            return res.status(404).json({ error: 'Utilisateur non trouvé' });
        }

        res.status(200).json({ message: 'ID client Stripe mis à jour avec succès' });
    } catch (error) {
        console.error('Erreur lors de la mise à jour de l\'ID client Stripe : ', error);
        res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'ID client Stripe' });
    }
};
