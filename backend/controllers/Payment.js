const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const User = require('../models/User');

exports.createPayment = async (req, res) => {
    const { amount } = req.body;

    try {
        const paymentIntent = await stripe.paymentIntents.create({
            amount,
            currency: 'eur', // Changez la devise si nécessaire
        });

        res.status(200).send({
            clientSecret: paymentIntent.client_secret,
        });
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
};

exports.createCustomer = async (req, res) => {
    const { email, name, userId } = req.body;

    try {
        const customer = await stripe.customers.create({
            email,
            name,
        });

        // Mettre à jour l'utilisateur dans la base de données avec le stripeCustomerId
        await User.findByIdAndUpdate(userId, { stripeCustomerId: customer.id });

        res.status(200).send({
            customerId: customer.id,
        });
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
};

exports.createSubscription = async (req, res) => {
    const { customerId, priceId, userId } = req.body;

    console.log('Received customerId:', customerId);
    console.log('Received priceId:', priceId);

    if (!customerId || !priceId) {
        return res.status(400).send({ error: 'CustomerId et priceId sont requis.' });
    }

    try {
        const subscription = await stripe.subscriptions.create({
            customer: customerId,
            items: [{ price: priceId }],
            payment_behavior: 'default_incomplete',
            expand: ['latest_invoice.payment_intent'],
        });

        // Mettre à jour l'utilisateur pour dire qu'il est abonné
        await User.findByIdAndUpdate(userId, { subscribe: true });

        res.status(200).send({
            subscriptionId: subscription.id,
            clientSecret: subscription.latest_invoice.payment_intent.client_secret,
        });
    } catch (error) {
        console.error('Error creating subscription:', error);
        res.status(500).send({ error: error.message });
    }
};