const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')
const {ObjectId} = require("mongoose/lib/types");

const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

const userSchema = mongoose.Schema({
    email: {type: String, required: true, unique: true, match: emailRegex},
    password: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    subscribe: {type: Boolean, required: true, default: false},
    imageURL: {type: String, required: false},
    stripeCustomerId: {type: String, required: false}
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
