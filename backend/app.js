require('dotenv').config();

const express = require('express');
const app = express();
const serveStatic = require('serve-static');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/User');
const paymentRoutes = require('./routes/Payment');

const allowedOrigins = [
    'http://localhost:8080',
    'https://generateur-qrcode.com',
    'https://api.generateur-qrcode.com'
];

app.use(cors({
    origin: function (origin, callback) {
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
            const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    },
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
}));

app.options('*', cors());


const username = encodeURIComponent('loic3357');
const password = encodeURIComponent('Bordeaux3357');
const cluster = 'cluster0.4jptkmi.mongodb.net';
const dbname = 'test';

const uri = `mongodb+srv://${username}:${password}@${cluster}/${dbname}?retryWrites=true&w=majority`;

mongoose.connect(uri)
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch((error) => console.error('Connexion à MongoDB échouée !', error));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api/auth', userRoutes);
app.use('/api/stripe', paymentRoutes);
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res) => {
    res.json({ message: 'requête bien reçue !' });
});

app.listen(process.env.PORT || 3000);

app.use('/', serveStatic(path.join(__dirname, '/dist')))

app.get(/.*/, function (req, res) {
    res.sendFile(path.join(__dirname, '/dist/index.html'))
})
