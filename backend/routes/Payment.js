const express = require('express');
const router = express.Router();
const paymentController = require('../controllers/Payment');
const auth = require('../middleware/auth');

// Paiement simple
router.post('/create-payment', auth, paymentController.createPayment);

// Souscription
router.post('/create-customer', paymentController.createCustomer);
router.post('/create-subscription', auth, paymentController.createSubscription);

module.exports = router;