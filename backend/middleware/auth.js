const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;

        if (!authHeader) {
            return res.status(401).json({ error: 'Token manquant' });
        }

        const token = authHeader.split(' ')[1]; // Récupérer le token JWT depuis l'en-tête Authorization
        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET'); // Décoder et vérifier le token JWT

        req.user = { _id: decodedToken.userId }; // Ajouter l'ID de l'utilisateur décodé à req
        next(); // Passer à l'étape suivante du middleware
    } catch (error) {
        res.status(401).json({ error: 'Token invalide ou expiré' }); // En cas d'erreur, renvoyer une erreur 401
    }
};
